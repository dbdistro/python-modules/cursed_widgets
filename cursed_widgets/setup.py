

from setuptools import setup


setup(
    name="cursed_widgets",
    version="2024.12.26.R3",
    packages=["cursed_widgets"],
    description="Use to easily create widgets/TUI windows with the curses module.",
    author="Marcus Ed. Butler \"HackerTheFox\"",
    author_email="marcus@distrobutler.com",
    url="https://gitlab.com/dbdistro/python-modules/cursed_widgets",
)
