#/usr/bin/env python3

"""
CREATOR: Marcus Ed. Butler "HackerTheFox"
DESCRIPTION: "Refer to setup.py"
LAST_VERSION: "Refer to setup.py"
CURRENT_VERSION: "Refer to setup.py"
"""
# USE:
# from cursed_widgets import Widgets

import curses, time


class Widgets:
    def __init__(self, stdscr, startX=0, startY=0, width=None, height=None, title=None):
        # Set up width and height if set to None.
        # Gets the size of the terminal/cmdline.
        height_of_win, width_of_win = stdscr.getmaxyx()

        resizable = False
        if width == None:
            width = width_of_win
            resizable = True
        if height == None:
            height = height_of_win
            resizable = True


        self.resizable = resizable
        self.screen = stdscr
        self.startX = startX
        self.startY = startY
        self.width = width
        self.height = height
        self.title = title
        self._active = None

        # Hide cursor
        curses.curs_set(False)
        # Turn colors on.
        curses.start_color()

        """COLORS"""
        # Windows
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        # Desktop
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_RED)
        # Select
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)
        # Text Entry
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_WHITE)
        # Title
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_RED)
        # Active Widget
        curses.init_pair(6, curses.COLOR_RED, curses.COLOR_WHITE)

        self.widgets = {}

        """TEXT WIDGET"""
        self.left = False
        self.right = False
        self.up = False
        self.down = False

        self.subtract = 0

    ''' Clears everything off the screen and does a redraw and refresh. '''
    def refresh_screen(self):
        self.screen.erase()
        self.draw()
        self.screen.refresh()        


    def update(self):
        # Refresh the screen and capture events.
        self.refresh_screen()
        key = self.screen.getch()
        self._update(key)
        
        return key
    
    
    def focus(self, widget_name):
        # Assigns widget to be in control by user.
        widget = self.widgets[widget_name]
        self._active = widget_name
        if widget['type'] == "textBox":
            self.screen.nodelay(True)
        else:
            self.screen.nodelay(False)
        while True:
            key = self.update()
            # Selection or keybinding pressed, return result.
            # MsgBox with no keybindings will not remain active.
            if widget['type'] == 'msgBox' and widget['keybinds'] == None:
                break

            # If there are keybindings return them if they are pressed.
            if widget['keybinds'] != None and key != -1:
                if chr(key) in widget["keybinds"]:
                    widget["results"] = chr(key)
            # Return result from widget. EX: yesnoBox widget, user selects no, widget["results"] = "No"
            if widget["results"]:
                break

            
        results = widget["results"]
        widget["results"] = None
        self.screen.nodelay(True)
        self._active = None
        self.refresh_screen()

        return results


    # Gets text to fit inside of a widget.
    def makeTextFit(self, width, height, text):
        text_split_into_words = text.split()
        adjusted_text = ""
        x_position = 0
        for word in text_split_into_words:
            # Check if word will fit on line.
            # If not go down a line and continue.
            if x_position + len(word) < width:
                adjusted_text += word + ' '
                x_position += len(word) + 1
            else:
                x_position = 0
                try:
                    adjusted_text += '\n'
                    adjusted_text += word + ' '
                    x_position += len(word) + 1
                except:
                    break    

        # Check if text will fit in widget.
        # If not throw an error -1.
        # if width * height < len(adjusted_text):
        #     raise Exception("Message will not fit in the widget. Either change the wording in the msg or change the widgets size. Check your widget creation function. EX: widgets.msgBox() widgets.yesnoBox()")
        
        return adjusted_text
                 

    ''' Used to change the loading widgets parameter load.'''
    def load(self, integer, widget):
        self.widgets[widget]["load"] = integer
        self.refresh_screen()


    ''' Used to show/hide widgets from user's view. '''
    def show(self, boolean, widget):
        self.widgets[widget]["show"] = boolean
        self.refresh_screen()

                               
################################
# Create Widget Functions
################################
    def okBox(self, name, x, y, msg, width=None, height=None, ok='Ok', keybinds=None, order=0):
        msg = str(msg)
        
        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'msg': msg, 'type': 'okBox', 'results': None, 'keybinds':keybinds, 'order': order, 'show':False, 'ok': ok}

        # Tries to get the msg box to fit on 1 line. If not create additional lines.
        # Changes size of msgBox to get it to fit.
        if width == None and height == None:
            ## Try to make the width the size of the string.
            if len(msg) + x < self.width:
                width = len(msg) + 1
                height = 1
                
            else:
                # If the msgBox needs height.
                unprocessed_length = len(msg) + 1
                height = 0
                # Have all of the characters in msg been processed?
                while unprocessed_length > 0:
                    unprocessed_length -= self.width - x
                    height += 1
                # Width and Height found. Width is equal to screens width.
                width = self.width - x - 3

        # If height not given.
        elif height == None:
            # width = 10
            # len(msg) = 20

            unprocessed_length = len(msg)
            height = 0
            while unprocessed_length >= 0:
                unprocessed_length -= width
                height += 1
        
        # If width not given.
        elif width == None:
            width = self.width - x - 3


        widget['width'] = width
        widget['height'] = height + 2
                
                
        ## If text will not fit on line do to screen space. Make lines until the string will fit.
        ## If it will not fit on the screen throw an error.


        widget['msg'] = self.makeTextFit(width, height, msg)
        self.widgets[name] = widget


    def msgBox(self, name, x, y, msg, width=None, height=None, keybinds=None, order=0):
        msg = str(msg)
        
        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'msg': msg, 'type': 'msgBox', 'results': None, 'keybinds':keybinds, 'order': order, 'show':False}

        # Tries to get the msg box to fit on 1 line. If not create additional lines.
        # Changes size of msgBox to get it to fit.
        if width == None and height == None:
            ## Try to make the width the size of the string.
            if len(msg) + x < self.width:
                width = len(msg) + 1
                height = 1
                
            else:
                # If the msgBox needs height.
                unprocessed_length = len(msg) + 1
                height = 0
                # Have all of the characters in msg been processed?
                while unprocessed_length > 0:
                    unprocessed_length -= self.width - x
                    height += 1
                # Width and Height found. Width is equal to screens width.
                width = self.width - x - 3

        # If height not given.
        elif height == None:
            # width = 10
            # len(msg) = 20

            unprocessed_length = len(msg)
            height = 0
            while unprocessed_length >= 0:
                unprocessed_length -= width
                height += 1
        
        # If width not given.
        elif width == None:
            width = self.width - x - 3


        widget['width'] = width
        widget['height'] = height
                
                
        ## If text will not fit on line do to screen space. Make lines until the string will fit.
        ## If it will not fit on the screen throw an error.


        # widget['msg'] = self.makeTextFit(width, height, msg)
        self.widgets[name] = widget


    def yesnoBox(self, name, x, y, msg, width=None, height=None, yes='Yes', no='No', keybinds=None, order=0):
        msg = str(msg)

        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'msg': msg, 'yes': yes, 'no': no, 'select': no, 'type': 'yesnoBox', 'results': None, 'keybinds':keybinds, 'order': order, 'show':False}


       # Tries to get the message to fit on 1 line. If not create additional lines.
        # Changes size of yesnoBox to get it to fit.
        if width == None and height == None:
            ## Try to make the width the size of the string.
            if len(msg) + x < self.width:
                width = len(msg) + 1
                height = 3
                
            else:
                # If the yesnoBox needs height.
                unprocessed_length = len(msg) + 1
                height = 3
                # Have all of the characters in msg been processed?
                while unprocessed_length > 0:
                    unprocessed_length -= self.width - x
                    height += 1
                # Width and Height found. Width is equal to screens width.
                width = self.width - x - 3

        # If height not given.
        elif height == None:
            # If the msg will fit on one line.
            if len(msg) + x < self.width and len(msg) <= width:
                height = 3
            else:
                # If the msgBox needs height.
                unprocessed_length = len(msg) + 1
                height = 3
                # Have all of the characters in msg been processed?
                while unprocessed_length > 0:
                    unprocessed_length -= width
                    height += 1
                # Width and Height found. Width is equal to screens width.
        
        # If width not given.
        elif width == None:
            width = self.width - x - 3

        widget['width'] = width
        widget['height'] = height
 

        
        widget['msg'] = self.makeTextFit(width, height - 2, msg)
        self.widgets[name] = widget
        

    def formBox(self, name, x,  y, questions, width=None, height=None, button="Ok", keybinds=None, order=0):
        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'questions': {}, 'select': questions[0], "type": "formBox", "largest question": 0, "button": button, 'results': None, 'keybinds':keybinds, 'order': order, 'show':False}

        # Create empty entry for questions and figure out which question is the longest.
        maxSize = 0
        for question in questions:
            widget['questions'][question] = ""

            if len(question) > maxSize:
                maxSize = len(question)
        widget['largest question'] = maxSize

        if width == None:
            widget['width'] = maxSize + 30
        if height == None:
            widget['height'] = len(questions) + 2
        
        self.widgets[name] = widget

        
    def selectBox(self, name, x, y, raw_choices, title=None, width=None, height=None, button="Ok", maxItems=7, keybinds=None, multiple_choices=False, order=0):
        widget = {'name': name, 'x': x, 'y': y, 'choices': [], 'width': width, 'height': height, 'select': 0, 'page': 0, "maxItems": maxItems, 'results': None, 'checked': None, 'button': button, "rightArrow": "-->", "leftArrow": "<--", 'type':'selectBox', 'keybinds': keybinds, 'order': order, 'show': False, 'multiple_choices': multiple_choices}


        if widget["multiple_choices"]:
            widget["checked"] = []

            # Gives user option to have some checked. EX: [*] apples or [ ] apples. [1st value choice, 2nd value True|False]
            new_raw_choices = []
            index = 0
            for choice in raw_choices:
                if type(choice) == list:
                    if choice[1]:
                        # Check by default
                        widget["checked"].append(choice[0])
                    new_raw_choices.append(choice[0])
                else:
                    new_raw_choices.append(choice)
                index += 1
            raw_choices = new_raw_choices
                    

        # is this a list?
        ## if so: append all checked items to checked
        maxSize = 0

        if title != None:
            widget['title'] = f"{title}:"
            maxSize = len(widget['title'])
        else:
            widget['title'] = None

        if width == None:
            # Get longest string.
            for choice in raw_choices:
                if len(choice) > maxSize:
                    maxSize = len(choice)

            widget['width'] = maxSize + 10

        if height == None:
            # Create right/left arrow if len is greater than 7.
            if len(raw_choices) > maxItems:
                widget['height'] = maxItems + 4
            else:
                widget['height'] = len(raw_choices) + 4

            if widget["title"] != None:
                widget['height'] + 4

        # Create choices, divide into groups of 7.
        choices = []
        group = []
        num = 0
        for choice in raw_choices:
            if num >= maxItems:
                group.append(choice)
                choices.append(group)
                group = []
                num = 0
            else:
                group.append(choice)
                num += 1
        # Will get choices that were not equal to 7 that.
        if len(group) != 0:
            choices.append(group)

        widget["choices"] = choices
                
        # Make choice equal to 1st option.
        widget["select"] = widget["choices"][0][0]

        self.widgets[name] = widget
        

    def loadingBox(self, name, x, y, width=None, height=4, load=0, order=0):
        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'load': load, 'type': 'loadingBox', 'results': None, 'keybinds': None, 'order': order, 'show': False}

        # If no width specified.
        if width == None:
            widget['width'] = self.width - 2 - (x * 2)

        self.widgets[name] = widget


    def textBox(self, name, x, y, width=None, height=None, order=0):
        widget = {'name': name, 'x': x, 'y': y, 'width': width, 'height': height, 'text': [[]], 'right_scroll': 0, 'down_scroll': 0, 'on_screen_cursor_position': [0, 0], 'cursor_position': [0, 0], "lowest_line": 0, 'type': "textBox", "results": None, 'keybinds': None, 'order': order, 'show': False}

        # If no width or height given.
        if width == None:
            width = self.width - x
        if height == None:
            height = self.height - y

        widget['width'] = width - 3
        widget['height'] = height - 3

        self.widgets[name] = widget


################################
# Helper Fuctions
################################
    def _update(self, key):
        for widget in self.widgets.keys():
            if widget == self._active:
                widget = self.widgets[widget]
                if widget['type'] == "okBox":
                    if key == curses.KEY_ENTER or key == 10 or key == 13:
                        widget['results'] = widget['ok']                        
                if widget['type'] == 'yesnoBox':
                    if key == curses.KEY_RIGHT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_LEFT:
                        if widget['select'] == widget['yes']:
                            widget['select'] = widget['no']
                        else:
                            widget['select'] = widget['yes']
                    elif key == curses.KEY_ENTER or key == 10 or key == 13:
                        widget['results'] = widget['select']

                elif widget['type'] == "formBox":
                    questions = list(widget['questions'].keys())
                    if widget['select'] != widget['button']:
                        if key == curses.KEY_UP:
                            widget['select'] = questions[questions.index(widget['select'])-1]
                        elif key == curses.KEY_DOWN:
                            if questions.index(widget['select']) == len(questions) - 1:
                                widget['select'] = questions[0]
                            else:
                                widget['select'] = questions[questions.index(widget['select'])+1]
                    if key == 9:
                        # select button tab key
                        if widget['select'] != widget['button']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = questions[0]
                    if key == 10 or key == 13:
                        if widget['select'] == widget['button']:
                            widget['results'] = widget['questions']
                        else:
                    
                            # text entry mode
                            x = widget['x'] + widget['largest question'] + 2 + len(widget['questions'][widget['select']])
                            while True:
                                self.screen.erase()
                                self.draw()
                                self.screen.refresh()
                                self.screen.addch(widget['y'] + questions.index(widget['select']), x, '█', curses.color_pair(4) | curses.A_BLINK)
                                key = self.screen.getkey()
                                if key == "KEY_BACKSPACE" and len(widget['questions'][widget['select']]) != 0:
                                    widget['questions'][widget['select']] = widget['questions'][widget['select']][:-1]
                                    x -= 1
                                # Exit text mode
                                elif key == "\n":
                                    break
                                
                                # input text goes to widgets dictionary
                                elif len(widget['questions'][widget['select']]) <  widget['width'] - widget["largest question"] - 3 and key != "\t" and "KEY" not in key:
                                    widget['questions'][widget['select']] += key
                                    x += 1                       
            
                elif widget['type'] == "selectBox":
                    # Down Key
                    if key == curses.KEY_DOWN and widget['select'] != widget['button'] and widget["select"] != widget["rightArrow"] and widget["select"] != widget["leftArrow"]:
                        if widget["choices"][widget["page"]].index(widget["select"]) + 1 == len(widget["choices"][widget["page"]]):
                            widget["select"] = widget["choices"][widget["page"]][0]
                        else:
                            widget["select"] = widget["choices"][widget["page"]][widget["choices"][widget["page"]].index(widget["select"]) + 1]
                    # Up Key
                    if key == curses.KEY_UP and widget['select'] != widget['button'] and widget["select"] != widget["rightArrow"] and widget["select"] != widget["leftArrow"]:
                        widget['select'] = widget["choices"][widget["page"]][widget["choices"][widget["page"]].index(widget["select"]) - 1]
                    # Enter key
                    if key == 10 or key == 13 or key == curses.KEY_ENTER:
                        # PRESS BUTTON
                        if widget['select'] == widget['button']:
                            if widget['multiple_choices'] and widget['checked'] == []:
                                widget["results"] = 1
                            else:
                                widget['results'] = widget['checked']
                        
                        # PRESS LEFT ARROW
                        elif widget["select"] == widget["leftArrow"]:
                            if widget["page"] == 0:
                                widget["page"] = len(widget["choices"]) -1
                            else: 
                                widget["page"]-= 1
                        # PRESS RIGHT ARROW
                        elif widget["select"] == widget["rightArrow"]:
                            if widget["page"] == len(widget["choices"]) -1:
                                widget["page"] = 0
                            else:
                                widget["page"]+= 1
                        else:
                            # Check mark the option
                            if widget["multiple_choices"] == False:
                                widget['checked'] = widget['select']
                            else:
                                if widget['select'] in widget['checked']:
                                    # Remove - Uncheck
                                    widget['checked'].remove(widget['select'])
                                else:
                                    # Add to list - Check
                                    widget['checked'].append(widget['select'])
        
                    # Tab key
                    if key == 9:
                        # select button button
                        if widget['select'] != widget['button'] and widget['select'] != widget['rightArrow'] and widget['select'] != widget['leftArrow']:
                            widget['select'] = widget['button']
                        else:
                            widget['select'] = widget['choices'][widget["page"]][0]
                            
                    # Left ok right
                    ## Get the amount of choices.
                    length_of_choices = 0
                    for choices in widget["choices"]:
                        length_of_choices += len(choices)
                    if length_of_choices > widget["maxItems"]:
                        if key == curses.KEY_LEFT:
                            if widget["select"] == widget["button"]:
                                widget["select"] = widget["leftArrow"]
                            elif widget["select"] == widget["leftArrow"]:
                                widget["select"] = widget["rightArrow"]
                            elif widget["select"] == widget["rightArrow"]:
                                widget["select"] = widget["button"]
                        elif key == curses.KEY_RIGHT:
                            if widget["select"] == widget["button"]:
                                widget["select"] = widget["rightArrow"]
                            elif widget["select"] == widget["rightArrow"]:
                                widget["select"] = widget["leftArrow"]
                            elif widget["select"] == widget["leftArrow"]:
                                widget["select"] = widget["button"]
                    
                    
                elif widget['type'] == "textBox":
                    # Text Box mode
                    self.screen.nodelay(False)
                    while True:
                        self.screen.erase()
                        self.draw()
                        key = self.screen.getkey()

                        # Debug
                        with open("TMP.debug", "w") as f_obj:
                            f_obj.write(str(key))

                        if key != "KEY_BACKSPACE":
                            self.subtract = 0
                        # Backspace key
                        if key == "KEY_BACKSPACE" and widget['cursor_position'] != [0, 0]:
                            # Check if cursor at beginning of line.
                            if widget["cursor_position"][0] == 0 and widget["cursor_position"][1] > 0:
                                # Grab the line the cursor is going to before it is altered.
                                backspaced_line_len = len(widget["text"][widget["cursor_position"][1] - 1])
                                # Append everything on line into the one above it.
                                self.subtract = 0 # Moves the cursor before the text backspaced up onto line.
                                for character in widget["text"][widget["cursor_position"][1]]:
                                    widget["text"][widget["cursor_position"][1] - 1].append(character)
                                    self.subtract += 1

                                del widget["text"][widget["cursor_position"][1]]

                            else:
                                del widget["text"][widget["cursor_position"][1]][widget["cursor_position"][0] - 1]
                            self.left = True

                        # Create a newline.
                        elif key == "\n":
                            # Creates a newline.
                            widget["text"].insert(widget["cursor_position"][1] + 1, [])

                            # Grab all text after cursor and put it into next line.
                            for character in widget["text"][widget["cursor_position"][1]][widget["cursor_position"][0]:]:
                                 widget["text"][widget["cursor_position"][1] + 1].append(character)
                            
                            # Delete all text after cursor.
                            del widget["text"][widget["cursor_position"][1]][widget["cursor_position"][0]:]
                            

                            # Updates cursor position
                            widget["cursor_position"][0] = 0
                            widget["cursor_position"][1] += 1
                            widget["on_screen_cursor_position"][0] = 0
                            widget["right_scroll"] = 0

                            # Checks if screen needs to be scrolled or cursor moved.
                            if widget["cursor_position"][1] + 1 > widget["height"]:
                                widget["down_scroll"] += 1
                            else:
                                widget["on_screen_cursor_position"][1] += 1

                        # Arrow keys
                        ## Left Key 260
                        elif key == "KEY_LEFT":
                            self.left = True
                        ## Right Key
                        elif key == "KEY_RIGHT":
                            self.right = True
                        ## Up Key
                        elif key == "KEY_UP":
                            self.up = True
                        ## Down Key
                        elif key == "KEY_DOWN":
                            self.down = True
                            
                        # Whatever key the user defines to exit the widget. *undecided.
                        elif key == "THIS WILL EXIT OUT OF WIDGET":
                            pass
                        
                        # input text goes to widgets dictionary
                        elif "KEY" not in key:
                            widget["text"][widget["cursor_position"][1]].insert(widget["cursor_position"][0], key)

                            self.right = True

                        
                        # Move Cursor to the left
                        if self.left:
                            self.left = False
                            # Do a check to see if cursor can go left.
                            if widget["cursor_position"] != [0,0]:
                                # Check if cursor at beginning of line.
                                if widget["cursor_position"][0] == 0 and widget["cursor_position"][1] > 0:
                                    # Check if going off of screen in y axis.
                                    if widget["on_screen_cursor_position"][1] -1 < 0:
                                        widget["down_scroll"] -= 1
                                    else:
                                        widget["on_screen_cursor_position"][1] -= 1

                                    # Change the cursor position
                                    widget["cursor_position"][0] = len(widget["text"][widget["cursor_position"][1] - 1]) - self.subtract 
                                    widget["cursor_position"][1] -= 1

                                    # Change the on screen cursor. (No charaters in line except \n, then the user decides to backspace. Go up to the next line.)
                                    if widget["cursor_position"][0] >= widget["width"]:
                                        # How much to scroll and where on the screen should the cursor go?
                                        #widget["right_scroll"] = len(widget["text"][widget["cursor_position"][1]]) - widget["width"] + 1
                                        # Only run this line of code if backspace key pressed.
                                        if key == "KEY_BACKSPACE":
                                            widget["right_scroll"] = backspaced_line_len - widget["width"] +1
                                        else:
                                            widget["right_scroll"] = len(widget["text"][widget["cursor_position"][1]]) - widget["width"] +1
                                            
                                        widget["on_screen_cursor_position"][0] = widget["width"] - 1
                                    else:
                                        # Only run this line of code if backspace key pressed.
                                        if key == "KEY_BACKSPACE":
                                            widget["on_screen_cursor_position"][0] = backspaced_line_len
                                        else:
                                            widget["on_screen_cursor_position"][0] = len(widget["text"][widget["cursor_position"][1]])
                                else:
                                    # Check if going off of screen.
                                    if widget["on_screen_cursor_position"][0] == 20 and widget["right_scroll"] >= 1:
                                        widget["right_scroll"] -= 1
                                    else:
                                        widget["on_screen_cursor_position"][0] -= 1        
                                    widget["cursor_position"][0] -= 1

                        # Move Cursor to the right
                        if self.right:
                            self.right = False
                            # Do a check to see if cursor can go right.
                            if widget["cursor_position"][0] < len(widget["text"][widget["cursor_position"][1]]):
                                if widget["on_screen_cursor_position"][0] + 2 > widget["width"]:
                                    widget["right_scroll"] += 1
                                else:
                                    widget["on_screen_cursor_position"][0] += 1
                                widget["cursor_position"][0] += 1

                            # See if cursor can move down a line. (Assuming there is a line under the cursor and the cursor is at the end of the line.)
                            elif widget["cursor_position"][1] + 1 != len(widget["text"]):
                                widget["cursor_position"][0] = 0
                                widget["cursor_position"][1] += 1

                                widget["right_scroll"] = 0
                                widget["on_screen_cursor_position"][0] = 0

                                # Check to see if change down_scroll or move the on_screen_cursor_position down.
                                if widget["on_screen_cursor_position"][1] + 1 >= widget["height"]:
                                    widget["down_scroll"] += 1
                                else:
                                    widget["on_screen_cursor_position"][1] += 1

                                
            
                        # Move cursor up.
                        if self.up:
                            self.up = False
                            # Check to see if the cursor can go up.
                            if widget["cursor_position"][1] != 0: # Can't go higher than the origin.
                                widget["cursor_position"][1] -= 1

                                # Check if cursor will be by text. If not shift over to the left.
                                if widget["cursor_position"][0] > len(widget["text"][widget["cursor_position"][1]]):
                                    # Move cursor to end of line.
                                    widget["cursor_position"][0] = len(widget["text"][widget["cursor_position"][1]])

                                    # If offscreen going left.
                                    if len(widget["text"][widget["cursor_position"][1]]) > widget["width"]:
                                        widget["right_scroll"] = len(widget["text"][widget["cursor_position"][1]]) - widget["width"] + 1
                                        widget["on_screen_cursor_position"][0] = widget["width"] - 1
                                    else:
                                        widget["right_scroll"] = 0
                                        widget["on_screen_cursor_position"][0] = len(widget["text"][widget["cursor_position"][1]])
                                        
            
                                # Is cursor goint up off of the top of widget.
                                if widget["on_screen_cursor_position"][1] == 0:
                                    widget["down_scroll"] -= 1
                                else:
                                    widget["on_screen_cursor_position"][1] -= 1


                        # Move cursor down.
                        if self.down:
                            self.down = False
                            # Check to see if cursor can go down.
                            # Can't go lower than the amount of rows/lines/newlines created.
                            if widget["cursor_position"][1] != len(widget["text"]) - 1:
                                widget["cursor_position"][1] += 1

                                # Check if cursor will be by text. If not shift over to the left.
                                if widget["cursor_position"][0] > len(widget["text"][widget["cursor_position"][1]]):
                                    # Move cursor to end of line.
                                    widget["cursor_position"][0] = len(widget["text"][widget["cursor_position"][1]])

                                    # If offscreen going left.
                                    if len(widget["text"][widget["cursor_position"][1]]) > widget["width"]:
                                        widget["right_scroll"] = len(widget["text"][widget["cursor_position"][1]]) - widget["width"] + 1
                                        widget["on_screen_cursor_position"][0] = widget["width"] - 1
                                    else:
                                        widget["right_scroll"] = 0
                                        widget["on_screen_cursor_position"][0] = len(widget["text"][widget["cursor_position"][1]])
                                        
                                # If cursor going off the bottom of the widget.
                                if widget["on_screen_cursor_position"][1] >= widget["height"] - 1:
                                    widget["down_scroll"] += 1
                                else:
                                    widget["on_screen_cursor_position"][1] += 1
                                    
                                
                
                elif widget['type'] == 'loadingBox':
                    pass

    
    def draw(self):
        # Draw background
        if self.resizable:
            self.screen.bkgd(' ', curses.color_pair(2))
        else:
            for height in range(self.height):
                self.screen.addstr(height, 0, ' '*(int(self.width) - 1), curses.color_pair(2))
        # Draw title
        if self.title != None:
            self.screen.addstr(0, 1, self.title, curses.color_pair(5) | curses.A_BOLD)
            self.screen.addstr(1, 1, "─"*(int(self.width) - 1), curses.color_pair(5) | curses.A_BOLD)

        # Puts widgets in a list that determines which ones get drawn first.
        widgets_dic = self.widgets.values()
        widgets = []
        for widget in widgets_dic:
            widgets.append(widget)
        
        ordered_widgets = []

        ## Get max value.
        max_value = 0
        
        if len(widgets) != 0:
            for widget in widgets:
                if widget['order'] > max_value:
                    max_value = widget['order']

        ## Put widgets in order.
        counter = 0
        while counter <= max_value:
            for widget in widgets[:]:
                if widget['order'] == counter:
                    ordered_widgets.append(widgets.pop(widgets.index(widget)))
            counter += 1
            
        
        # Draw widgets
        for widget in ordered_widgets:
            with open("TMP.debug", 'w') as f_obj:
                f_obj.write(str(f"{ordered_widgets}\n"))
        
            # Create border for widget
            # Create a different border around an active widget.
            if widget["show"]:
                if widget["name"] == self._active:
                    # widget = self.widgets[widget]
                
                    # Corner pieces
                    self.screen.addch(widget['y']-1, widget['x']-1, "┌", curses.color_pair(6))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] + widget['width'], "┘", curses.color_pair(6))
                    self.screen.addch(widget['y'] - 1, widget['x'] + widget['width'], "┐", curses.color_pair(6))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] - 1, "└", curses.color_pair(6))
        
                    # Top and bottom
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] - 1, x, "─", curses.color_pair(6))
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] + widget['height'], x, "─", curses.color_pair(6))
                    # Sides
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] - 1, "│", curses.color_pair(6))
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] + widget["width"], "│", curses.color_pair(6))                        
                else:
                    # widget = self.widgets[widget]
                    # Corner pieces
                    self.screen.addch(widget['y']-1, widget['x']-1, "┌", curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] + widget['width'], "┘", curses.color_pair(1))
                    self.screen.addch(widget['y'] - 1, widget['x'] + widget['width'], "┐", curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'], widget['x'] - 1, "└", curses.color_pair(1))

                    # Top and bottom
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] - 1, x, "─", curses.color_pair(1))
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(widget['y'] + widget['height'], x, "─", curses.color_pair(1))
                    # Sides
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] - 1, "│", curses.color_pair(1))
                    for y in range(widget['y'], (widget['y'] + widget['height'])): 
                        self.screen.addch(y, widget['x'] + widget["width"], "│", curses.color_pair(1))

                # Draw Shadows
                # Bottom
                for x in range(widget['x'], (widget['x'] + widget['width'] + 2)): 
                    self.screen.addch(widget['y'] + widget['height'] + 1, x, "█", curses.color_pair(1))          
                # Right
                for y in range(widget['y'], (widget['y'] + widget['height']) + 2): 
                                self.screen.addstr(y, widget['x'] + widget["width"] + 1, "██", curses.color_pair(1))
                               
                # Draw the box for the widget
                for y in range(widget['y'],  (widget['y'] + widget['height'])):
                    for x in range(widget['x'], (widget['x'] + widget['width'])): 
                        self.screen.addch(y, x, " ", curses.color_pair(1))
                                            
                # Adds buttons, messsages etc.
                if widget['type'] == "okBox":
                    numx = 0
                    numy = 0
                    for letter in widget["msg"]:
                        if letter == "\n":
                            numy += 1
                            numx = 0
                        else:
                            self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                            numx += 1    

                    self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - ((len(widget['ok']) +4) // 2) , f"< {widget['ok']} >", curses.color_pair(3) | curses.A_BOLD)
                
                elif widget['type'] == "msgBox":
                    numx = 0
                    numy = 0
                    for letter in widget["msg"]:
                        if letter == "\n":
                            numy += 1
                            numx = 0
                        else:
                            self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                            numx += 1


                    
                elif widget['type'] == "yesnoBox":
                    # bar seperating buttons from msg box.                    
                    numx = 0
                    numy = 0
                    for letter in widget["msg"]:
                        if letter == "\n":
                            numy += 1
                            numx = 0
                        else:
                            self.screen.addch(widget['y'] + numy, widget['x'] + numx, letter, curses.color_pair(1))
                            numx += 1

                    self.screen.addstr(widget['y'] + widget['height'] - 2, widget['x'], '─'*widget['width'], curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] - 1, '├', curses.color_pair(1))
                    self.screen.addch(widget['y'] + widget['height'] - 2, widget['x'] + widget['width'], '┤', curses.color_pair(1))
                    if widget['select'] == widget['yes']:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -5, f"< {widget['yes']} >", curses.color_pair(3) | curses.A_BOLD)
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - (len(widget['no']) + 1) +5, f"< {widget['no']} >", curses.color_pair(1) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - len(widget['yes']) -5, f"< {widget['yes']} >", curses.color_pair(1) | curses.A_BOLD)
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + (widget['width']) // 2 - (len(widget['no']) + 1) +5, f"< {widget['no']} >", curses.color_pair(3) | curses.A_BOLD)

                elif widget['type'] == "formBox":
                    
                    if widget['select'] == widget['button']:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, widget['x'] + widget['width'] // 2 - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))
                    num = 0
                    for key, value in widget['questions'].items():
                        question = key.rjust(widget['largest question'], " ")
                        question += ":"
                        if widget['select'] == key:
                            self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(3) | curses.A_BOLD)
                            self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1)) 
                        else:
                            self.screen.addstr(widget['y'] + num, widget['x'], question, curses.color_pair(1))
                            self.screen.addstr(widget['y'] + num, widget['x'] + len(question) + 1, value, curses.color_pair(1))
                        num += 1

                elif widget['type'] == "selectBox":
                    # Draw title
                    if widget['title'] != None:
                        self.screen.addstr(widget['y'], widget['x'], widget['title'], curses.color_pair(1))

                    # Draw button
                    if widget['select'] == widget["button"]:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(3) | curses.A_BOLD)
                    else:
                        self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - len(widget['button']), f"< {widget['button']} >", curses.color_pair(1))

                    # Right and left arrow
                    if len(widget["choices"]) > 1:
                        if widget['select'] == widget["rightArrow"]:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) + 9 - len(widget['button']), widget["rightArrow"], curses.color_pair(3) | curses.A_BOLD)
                        else:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) + 9 - len(widget['button']), widget["rightArrow"], curses.color_pair(1))
                        if widget["select"] == widget["leftArrow"]:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - 9 - len(widget['button']), widget["leftArrow"], curses.color_pair(3) | curses.A_BOLD)                    
                        else:
                            self.screen.addstr(widget['y'] + widget['height'] - 1, (widget['x'] + widget['width'] // 2) - 9 - len(widget['button']), widget["leftArrow"], curses.color_pair(1))                    

                    # Draw choices
                    if widget['title'] == None:
                        num = 0
                    else:
                        num = 2
                    for choice in widget['choices'][widget['page']]:
                        if widget["multiple_choices"]:
                            if choice == widget['select']:        
                                if choice in widget['checked']:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" [*] {choice}", curses.color_pair(3) | curses.A_BOLD)
                                else:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" [ ] {choice}", curses.color_pair(3) | curses.A_BOLD)
                            elif choice in widget['checked']:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" [*] {choice}", curses.color_pair(1))                    
                            else:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" [ ] {choice}", curses.color_pair(1))
                        else:
                            if choice == widget['select']:
                                if choice == widget['checked']:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(3) | curses.A_BOLD)
                                else:
                                    self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(3) | curses.A_BOLD)
                            elif choice == widget['checked']:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" (*) {choice}", curses.color_pair(1))                    
                            else:
                                self.screen.addstr(widget['y'] + num, widget['x'], f" ( ) {choice}", curses.color_pair(1))
                        num += 1


                elif widget['type'] == "textBox":
                    # Draw Text
                    ## How many lines can be drawn.
                    line_on = 0
                    character_x = 0
                    for y in range(widget["down_scroll"], widget["height"] + widget["down_scroll"]):
                        if line_on >= widget["lowest_line"]:
                            # Draw the characters on the line.
                            for x in range(widget["right_scroll"], widget["width"] + widget["right_scroll"]):
                                try:
                                    character = widget["text"][y][x]
                                except:
                                    break
                                self.screen.addch(widget["y"] + line_on, widget["x"] + character_x, str(character), curses.color_pair(1))
                                character_x += 1
                            character_x = 0
                        line_on += 1
                    ## How many columns can be drawn.
                    
                    # Draw Cursor
                    try:
                        self.screen.addch(widget["y"] + widget["on_screen_cursor_position"][1], widget["x"] + widget["on_screen_cursor_position"][0], str(widget["text"][widget["cursor_position"][1]][widget["cursor_position"][0]]), curses.color_pair(2) | curses.A_BLINK)
                    except:
                        self.screen.addch(widget["y"] + widget["on_screen_cursor_position"][1], widget["x"] + widget["on_screen_cursor_position"][0], ' ', curses.color_pair(2) | curses.A_BLINK)                        

                    self.screen.addstr(1, 1, str(widget["text"]))


                elif widget['type'] == "loadingBox":
                    for y in range(widget['height']):
                        self.screen.addstr(widget['y'] + y, widget['x'], " "*(int((widget["load"]/100) * widget["width"])))

            
# Create the widget give info: x y width height
# Draw widget
# Check for events within widget


if __name__ == "__main__":
    # Test out module.
    def run(screen):
        curses.start_color()
        curses.curs_set(False)
        screen.keypad(True)
        key = None
        height, width = screen.getmaxyx()
        
        widgets = Widgets(screen, title="This is an example")
        
        # widgets.msgBox("output", 10, 10, "This is a message box.", width=10, keybinds=("a"))
        # widgets.yesnoBox("cookie?", 20, 20, 50, 5, "Would you like some cookies? You can choose Yes or no. But why would you say no?")
        # widgets.selectBox("fav_character_question", 20, 10, ["Sonic", "Tails", "Knuckles", "Shadow"], title="Whose your favorite Sonic character?", multiple_choices=True)

        # widgets.msgBox('hint', 5, 118, 25, 9, "Keys:\n\n\t*Don't forget to hit ok\n\t\tbefore navigating to\n\t\tanother question.\n\n\t  TAB = Ok\n\t   F2 = Navigation\n\tENTER = Select/Action")

        # Install script test
        # widgets.msgBox("install text", width//2-40, height //2+10, 80, 10, "")
        # widgets.loadingBox("install load", width //2 - 30, height //2)
        # widgets.widgets["install text"]["msg"] = f"Partitioning drive...\n\n/dev/sda\n├── /dev/sda1\n├── /dev/sda2\n└── /dev/sda3"
        # widgets.widgets["install text"]["show"] = True                    
        # widgets.widgets["install load"]["show"] = True                    
        # widgets.widgets["install load"]["load"] = 0


        # Form
        #widgets.msgBox("greeting", 10, 13, 12, 5, "Hello World")
        # widgets.formBox("contact info", 10, 10, ("Name", "Phone #", "E-mail"), button="Submit")

        # Text Box
        #widgets.textBox("text", width//2 - 20, height//2 - 5, 30, 10)  

        # Ok box
        # widgets.okBox("text", width//2 - 10, height//2, "Sonic is the fastest thing alive!", ok="Alright")
        widgets.okBox("text", width//2 - 10, height//2, "Sonic is the fastest thing alive!")
        
        # Program Loop
        while True:
            widgets.widgets["text"]["show"] = True
            widgets.focus("text")

            exit()
            

            # screen.clear()
            
#             widgets.widgets["question"]["show"]= True
#             boolean = widgets.focus("question")
# # 
            # widgets.widgets["output"]["msg"] = str(boolean)
            # message box widget
            # widgets.widgets["contact info"]["show"]= True
            # choices = widgets.focus("contact info")
            # with open("TMP.debug", "w") as f:
            #     f.write(str(choices))
            # break
# 
#             # yes or no widget
#             widgets.widgets["cookie?"]["show"]= True
#             widgets.focus("cookie?")

            # Load Widget Change
#             while widgets.widgets["install load"]["load"] < 60:
#                 widgets.widgets["install load"]["load"] += 5
#                            
#                 widgets.draw()
#                 screen.refresh()
# 
#                 time.sleep(1)

#             # Form
#             widgets.widgets["contact info"]["show"] = True
#             widgets.widgets["greeting"]["show"] = True
# 
#             widgets.widgets["greeting"]["order"] = 1
# 
#             widgets.focus("contact info")

            # Text Box
            # widgets.widgets["text"]["show"] = True
            # widgets.focus("text")

    curses.wrapper(run)
