# cursed_widgets.py (BETA)

## A python3 module designed to create fancy widgets/TUI windows in the cmd line.

**Note:** This is `NOT` a terminal multiplexer.

**(NOTE: Instructions are for release to come. This project is still in beta. It is recommended to wait till next update, major changes will be made)**

<br/>

![](screenshots/lotsOfWidgets.png)

```python
import curses
from cursed_widgets import Widgets


def run(stdscr):

    widgets = Widgets(stdscr)

    # Creates a message box widget called message.
    widgets.msgBox("message", 2, 3, 13, 1, "Hello World!")
    widgets.formBox("form", 20, 3, ("Name", "Phone #", "Favorite Video Game"), button="Submit")
    widgets.yesnoBox("yesno", 48, 5, 23, 5, "Do you like DOOM?", no="Yes", yes="No")
    widgets.selectBox("select", 3, 12, ("Red", "Green", "Blue"), title="Whats your favorite color?")
    widgets.selectBox("select2", 35, 15, ("Red", "Green", "Blue", "Black", "Pink"), maxItems=3, title="Whats your favorite color combination?", multiple_choices=True)


    widgets.widgets["message"]["show"] = True
    widgets.widgets["form"]["show"] = True
    widgets.widgets["yesno"]["show"] = True
    widgets.widgets["select"]["show"] = True
    widgets.widgets["select2"]["show"] = True

    favorite_colors = widgets.focus("select2")


curses.wrapper(run)
```

# Tested on:

- Arch Linux

# Dependencies

If you are using **Linux** these dependencies should already be satisfied.

- python >= 3.7?
- curses module

### Why was this created?

<p>I wanted an easy way to build TUI windows in the terminal in python3. I could not find any programs that accomplished this.</p>
<br/>

## Table Of Context

### Installation

[Arch Linux](#arch-linux)

[Other Linux Systems](#other-linux-systems)

[Other Operating Systems](#other-operating-systems)

### Theme / Title Bar / Setup How to Setup

[Setup/Init](#setup--init)<br/>

### Create Widgets / TUI Windows

[Widgets / TUI Windows Introduction](#widgets--tui-windows-introduction)<br/>[Message Box](#message-box-msg-box)<br/>[Loading Box](#loading-box)<br/>[Form Box](#form-box)<br/>[Ok Box](#ok-box)<br/>[Yes No Box](#yes-no-box)<br/>[Select Box](#select-box)<br/>

### After Creating Widgets / TUI Windows

[How to change a widgets attribute after creation?](#how-to-change-a-widgets-attribute-after-creation)<br/>
[How to give a widget focus or make it active?](#how-to-give-a-widget-focus-or-make-it-active)<br/>

# Installation

## Arch Linux

Originally built to run on. Can be easily installed by adding my repository to your pacman.conf, then installing the package.

### 1. Add this repo to your pacman.conf

Follow this guide: [dbDistro Repository](https://gitlab.com/dbdistro/dbdistro-repo#dbdistro-repository)

### 2. Install the package

```bash
sudo pacman -Sy python-cursed_widgets
```

## Other Linux Systems

### 1. Clone the repository

```bash
git clone https://gitlab.com/dbdistro/python-modules/cursed_widgets.git
```

### 2. Install the package with pip

Make sure the directory you are in has a setup.py file:

```bash
cd cursed_widgets/cursed_widgets
pip install .
```

## Other Operating Systems

### Not Attempted Yet:

- Windows

- OpenBSD

- HaikuOS

- MacOS

# Setup / Init

<p>Your script should look like this at a minimum:</p>

```python
import curses
from cursed_widgets import Widgets


def run(stdscr):

    # Starts up TUI widget manager.
    widgets = Widgets(stdscr)

    # Create widgets here.

    # MAIN LOOP
    while True:
        # This is where your program runs.


curses.wrapper(run)
```

<p>At a minimum to init TUI widget manager you need to give it is your window object shown above. Note that running the following above will simply exit the program immediately. </p>

## All values for Widgets()

| Parameter | Required | Description                                                                                                                              |
| --------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| stdscr    | YES      | <p>Your curses window object.</p>                                                                                                        |
| startX    | NO       | <p>Default is 0. Changes the x position of the TUI widget manager on your terminal/cmdline.</p>                                          |
| startY    | NO       | <p>Default is 0. Changes the y position of the TUI widget manager on your terminal/cmdline.</p>                                          |
| width     | NO       | <p>Default is 0, which uses the whole terminal/cmdline space. Changes the width of the TUI widget manager on your terminal/cmdline.</p>  |
| height    | NO       | <p>Default is 0, which uses the whole terminal/cmdline space. Changes the height of the TUI widget manager on your terminal/cmdline.</p> |
| title     | NO       | <p>Adds a title to your TUI widget manager.</p> ![](screenshots/title.png)                                                               |

[Go to Table Of Context](#table-of-context)

# Widgets / TUI Windows

<br/>

# Widgets / TUI Windows Introduction

## Here is a list of parameters that all widgets have.

<p>These parameters will be used on widget creation and can be changed after the widget has been created.</p>

```python
widgets.NAME_OF_WIDGETBox("NAME OF WIDGET", x_position, y_position, width=None, height=None, keybinds=None, order=0)
```

### EXAMPLE

```python
widgets.msgBox("message", 10, 10, "This is a message box.", 28, 1, keybinds=('q'), order=0)
```

| Parameter Name | Required | Description                                                                                                                                                                                                                                                                                                              | Example           |
| -------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------- |
| name           | YES      | <p>Takes a string argument. Will be used when referencing the widget.</p>                                                                                                                                                                                                                                                | "message"         |
| x              | YES      | <p>Specifies the x coordinate the widget will be placed in.</p>                                                                                                                                                                                                                                                          | 10                |
| y              | YES      | <p>Specifies the y coordinate the widget will be placed in.</p>                                                                                                                                                                                                                                                          | 10                |
| width          | NO       | </p>Sets the width of the widget. If no width given will determine a width for the widget.</p><br/><br/>                                                                                                                                                                                                                 | 28                |
| height         | NO       | <p>Sets the height of the widget. If no height given will determine a width for the widget.</p>                                                                                                                                                                                                                          | 1                 |
| keybinds       | NO       | <p>Takes a tuple of characters. When the widget is in focus if one of the keys in the tuple is pressed will leave focus and return the key pressed.</p><p>This can be useful if you want to ask the user a yes or no question. Yes could be assigned as `'y'` and `'n'` could be assigned as no. keybinds=('y', 'n')</p> | ('q') or set('q') |
| order          | NO       | <p>Used to specify which widget goes on top. The argument has to be a intger from 0 to infinte. The larger the number the closer to the top it will be.</p><p>By default widgets are assigned a order of 0. If 2 widgets on top of each other have the same order number the newest one will be placed on top.</p>       | 0                 |

[Go to Table Of Context](#table-of-context)

# Form Box

## Displays a window with questions that will take typed answers.

### Create Widget

```python
widgets.formBox("NAME OF WIDGET", x_position, y_position, questions, width=None, height=None, button="Ok", keybinds=None, order=0)
```

### Example

![](screenshots/formWidget2.png)

```python
widgets.formBox("contact info", 10, 10, ("Name", "Phone #", "E-mail"), button="Submit")
```

| Parameter Name | Required | Description                                                                               | Example                                |
| -------------- | -------- | ----------------------------------------------------------------------------------------- | -------------------------------------- |
| questions      | YES      | Takes a tuple of strings. Each string will be shown as a question with its own text line. | ("Name", "Whats your favorite color?") |
| button         | NO       | Takes a string. By default will be "Ok". This changes the form's submit button text.      | button="Submit"                        |

<p>Outputs a dictionary with the key as the question and the answer as the item.</p>

[Go to Table Of Context](#table-of-context)

# Message Box (msg box)

## Displays a window with just text, nothing else.

### Create Widget

```python
widgets.msgBox("NAME OF WIDGET", x_position, y_position, msg, width, height, keybinds=None)
```

#### EXAMPLE

![](screenshots/msgWidget.png)

```python
widgets.msgBox("greeting", 1, 1, "Hello World, this is a message widget. (cool huh...)")
```

| Parameter Name | Required | Description                                                           | Example        |
| -------------- | -------- | --------------------------------------------------------------------- | -------------- |
| msg            | YES      | Takes a string type and will make the text appear in a simple window. | "Hello World!" |

[Go to Table Of Context](#table-of-context)

# Loading Box

## Displays a loading bar

### Create Widget

```python
widgets.loadingBox("NAME OF WIDGET", x_position, y_position, width=None, height=4, load=0, order=0)
```

#### Example

![](screenshots/loadingWidget.png)

```python
widgets.loadingBox("load", 20, 6, load=20)
```

| Parameter Name | Required | Description                                                                                      | Example                                    |
| -------------- | -------- | ------------------------------------------------------------------------------------------------ | ------------------------------------------ |
| load           | NO       | By default is 0. Takes an integer which decides how much of the bar is filled. 0=None 100=Filled | load=10 (which would fill 10% of the bar.) |

### Change how much of the bar is filled.

<p>To change load you could use this:</p>

```python
widgets.widgets["NAME OF WIDGET"]["load"] = ANY_NUMBER_BETWEEN_OR_EQUAL_TO_0_AND_100

widgets.draw()
stdscr.refresh()
```

A simpler way would be to use this function which runs the same code above but will result in less code to type.

```python
widgets.load(integer, "widget_name")
```

| Parameter Name | Required | Description                                                                                                                                                   | Example                              |
| -------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------ |
| integer        | YES      | Takes an integer, is the amount of space you want the bar filled. If you want 10% of the bar filled then you would put 10 as the argument for this parameter. | 10 (Which would be the same as 10%.) |
| widget         | YES      | Takes a string. Is the name of the loading widget you want to change.                                                                                         | "countdown"                          |

[Go to Table Of Context](#table-of-context)

## Ok Box

### Displays a widget with text and displays the button ok under it.

#### Create Widget

```python
widgets.okBox("widget name", x, y, msg, width=None, height=None, ok='Ok', keybinds=None, order=0)
```

#### Example

<img src="screenshots/okBoxWidget.png" title="" alt="" width="619">

```python
widgets.okBox("sonic is fast", 10, 10, "Sonic is the fastest thing alive!")
```

| Parameter Name | Required | Description                                                                                                                                                                                                                         | Example                             |
| -------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- |
| msg            | YES      | Takes a string. This is where text will go.                                                                                                                                                                                         | "Sonic is the fastest thing alive!" |
| ok             | NO       | Takes a string. Will change what the "< Ok >" button says.<br/>This will change what `widget.focus()` will return. By default the string "Ok" will be returned but if changed will return whatever you've changed this argument to. | ok="Alright"                        |

[Go to Table Of Context](#table-of-context)

## Yes No Box

### Displays a widget with text and displays the buttons yes and no under it.

#### Create Widget

```python
widgets.yesnoBox("widget name", x, y, msg, width=None, height=None, yes='Yes', no='No', keybinds=None, order=0)
```

#### Example

![](screenshots/yesnoWidget.png)

```python
widgets.yesnoBox("choice", 20, 10, "Did you complete your homework?")
```

| Parameter Name | Required | Description                                                                                                                                                     | Example                           |
| -------------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------- |
| msg            | YES      | Takes a string. This is where text will go.                                                                                                                     | "Did you complete your homework?" |
| yes            | NO       | Takes a string. Will change what the "< Yes >" button says. <br/>**NOTE:** This will change the return value on `widgets.focus()` function if this is selected. | yes="YEP"                         |
| no             | NO       | Takes a string. Will change what the "< No >" button says. <br/>**NOTE:** This will change the return value on `widgets.focus()` function if this is selected.  | no="NOPE"                         |

If yes or no parameter is used widgets.focus will output your string argument instead of the defaults. The output will be in the form of a string.

```python
import curses, time
from cursed_widgets import Widgets


def run(stdscr):
    widgets = Widgets(stdscr)

    # Create widgets.
    widgets.yesnoBox("homework_question", 20, 10, "Did you complete your homework?", yes="YEP", no="NOPE")
    widgets.msgBox("good", 20, 10, "Good, Now enjoy your free time.")
    widgets.msgBox("bad", 20, 10, "You should complete your homework before having free time.")

    while True:
        # Ask if homework was completed.
        # Show widget
        widgets.show(True, "homework_question")
        # Put widget into focus. When user selects a button returns
        # which button was pressed.    
        is_homework_complete = widgets.focus("homework_question")
        # Hide widget
        widgets.show(False, "homework_question")


        if is_homework_complete == "YEP":
            widgets.show(True, "good")
        else:
            widgets.show(True, "bad")

        time.sleep(5)

        widgets.show(False, "good")
        widgets.show(False, "bad")

curses.wrapper(run)
```

# Select Box

### Displays a window that allows a user to select a range of given choices.

#### Create Widget

```python
selectBox("widget name", x, y, raw_choices, title=None, width=None, height=None, button="Ok", maxItems=7, keybinds=None, multiple_choices=False, order=0)
```

#### Example

![](screenshots/selectBoxWidget.png)

```python
widgets.selectBox("fav_character_question", 10, 50, ("Sonic", "Tails", "Knuckles", "Shadow"), title="Whose your favorite Sonic character?")
```

| Parameter Name   | Required | Description                                                                                                                                                                                           | Example                                      |
| ---------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| raw_choices      | **YES**  | Takes a list or tuple of different choices the user can choose from.                                                                                                                                  | ("Sonic", "Tails")                           |
| title            | **NO**   | Sets the title of the window.                                                                                                                                                                         | title="Whose your favorite Sonic character?" |
| button           | **NO**   | Change what the button says.                                                                                                                                                                          | button="Confirm"                             |
| maxItems         | **NO**   | If there are too many choices to fit within the specified height then cursed_widgets creates additional pages to get the choices to fit. This argument specifies: "How many choices(items) per page?" | maxItems=10                                  |
| multiple_choices | **NO**   | When set to True allows the user to select more than one choice. When set to True returns a list instead of a string.                                                                                 | multiple_choices=True                        |

<p>Output will be a string unless `multiple_choices=True` then the output will be a list.</p>

---

<br/>

# How to give a widget focus or make it active?

<p>**An active or focused widget is a widget that the user can currently interact with.** For example can use keybindings assigned to that widget or answer a yes or no dialog. Also note that an active widget does not change its [order](#example). Also note that an active widget does not make it [visible](#visible).</p><p>Widgets by default have a black border, unless changed [theme](#nowhere). When a widget is in focus it will have a red border around it, again unless changed [theme](#nowhere).</p>

<p>To activate a widget simply call the focus function and give it the name of the widget. Your progam will pause while a widget is in focus. The program will resume if the widget loses focus for example you pressed one of the keys listed in the keybinds tuple.</p>

```python
widgets.focus("message")
```

## Keybinds with focus

<p>Like I said earlier the program will pause while this function is running. The focus function will return the key pressed if it was given as a keybind for that widget.</p>
<p>For example: Lets say Bob created a widget called message.Bob wants to be able to exit that message by pressing 'q'. Bob creates a widget with the keybind=('q').</p>

```python
widgets.msgBox("message", 2, 3, 23, 1, "Press 'q' to quit. :)", keybinds=('q'))
```

<p>Bob then types the following in his program:</p>

```python
letter = widgets.focus("message")

if letter == 'q':
    exit(0)
```

<p>The above code first creates a message box called message. Then we focus onto that widget and wait for a return. Then we check to see if the user typed 'q' if so exit the program. Obviously Bob does not need the if statement because you can't leave the focus on the widget message unless you type 'q', but I hope you can see how this works.</p>

### EXAMPLE

```python
import curses
from cursed_widgets import Widgets


def run(stdscr):
    widgets = Widgets(stdscr)

    # Creates a message box widget called message.
    widgets.msgBox("message", 2, 3, 23, 1, "Press 'q' to quit. :)", keybinds=("q"))


    widgets.widgets["message"]["show"] = True
    letter = widgets.focus("message")

    if letter == 'q':
        exit(0)

curses.wrapper(run)
```

[Go to Table Of Context](#table-of-context)

## How to change a widgets attribute after creation?

<p>To change a widgets attribute like its x and y position, or its width and height, or even things like the text in a message box, use:</p>

```python
widgets.widgets["widget_name_here"]["attribute_wanting_to_change_here"] = "value to set here"
```

### EXAMPLE

```python
widgets.widgets["message"]["show"] = True
```

<p>You must access widget values from the dictionary widgets.widgets which contains all widgets that you have created plus their attributes. Then to access the widget, type its name as the key for widgets.widgets, in the code above the name of the widget is "message". The next key can be any attribute you want to change in the example above its changing the widgets visibility, "show". Widget attributes can be changed at any time after the widget has been created.</p>

### More Examples

```py
widgets.widgets["message"]["x"] = 10

# This would only apply to msgBox widgets.
widgets.widgets["message"]["msg"] = "Hello World!"

# This would only apply to yesnoBox widgets.
widgets.widgets["anotherWidget"]["yes"] = "YEP"
```

```python
widgets.widgets["widget_name_here"]["attribute_wanting_to_change_here"] = "value to set here"
```

### Widget attributes that `cannot` be set during creation

| Attribute | Description                                                                                                   | Data Type     |
| --------- | ------------------------------------------------------------------------------------------------------------- | ------------- |
| show      | Can be used to show or hide a widget. Does not effect [focus](#how-to-give-a-widget-focus-or-make-it-active). | True or False |