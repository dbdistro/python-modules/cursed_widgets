import curses, time
from cursed_widgets import Widgets


def run(stdscr):
    # height, width = screen.getmaxyx()

    widgets = Widgets(stdscr)


    widgets.yesnoBox("homework_question", 20, 10, "Did you complete your homework?", yes="YEP", no="NOPE")
    widgets.msgBox("good", 20, 10, "Good, Now enjoy your free time.")
    widgets.msgBox("bad", 20, 10, "You should complete your homework before having free time.")

    while True:
        # Ask if homework was completed.
        widgets.show(True, "homework_question")
        is_homework_complete = widgets.focus("homework_question")
        widgets.show(False, "homework_question")
        

        if is_homework_complete == "YEP":
            widgets.show(True, "good")
        else:
            widgets.show(True, "bad")

        time.sleep(5)

        widgets.show(False, "good")
        widgets.show(False, "bad")
        
            
            

curses.wrapper(run)
